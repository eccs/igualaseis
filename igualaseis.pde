/*
	-------------------------------------------------------------------
	Igualaseis El Juego
	-------------------------------------------------------------------
	Copyright 2012 Jose M. Vega-Cebrian jmvegacebrian [at] gmail.com
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* @pjs preload="cadera.png, firmes.png, pin.png, bell.png,
roll00.png,
roll01.png,
roll02.png,
roll03.png,
roll04.png,
roll05.png,
roll06.png,
roll07.png,
roll08.png,
roll09.png,
roll10.png,
roll11.png,
roll12.png,
roll13.png,
roll14.png,
roll15.png,
roll16.png,
run00.png,
run01.png,
run02.png,
run03.png,
run04.png,
run05.png,
run06.png,
run07.png,
run08.png,
run09.png,
run10.png,
run11.png,
irun00.png,
irun01.png,
irun02.png,
irun03.png,
irun04.png,
irun05.png,
irun06.png,
irun07.png,
irun08.png,
irun09.png,
irun10.png,
irun11.png"; */
color BACKGROUND_COLOR = color(50);
int MODO_ROLL = 0;
int MODO_CADERA = 1;
int MODO_FIRMES = 2;
int MODO_RUN = 3;
int MODO_IRUN = 4;
int MODO_PIN = 5;

int M_WIDTH = 50;
int M_HEIGHT = 68;
int SCREEN_HEIGHT = 600;
int SCREEN_WIDTH = 1024;
int ESCENA_THRESHOLD = 10;

int _modo;

int m_menu = 2;

int escena;
boolean do_clear = false;

Animation monito_roll, monito_run, monito_irun;
PImage monito_firmes, monito_cadera, monito_bell, monito_pin;

ArrayList puntos;

void setup(){
	size(SCREEN_WIDTH,SCREEN_HEIGHT);
	background(BACKGROUND_COLOR);
	frameRate(24);

	monito_firmes = loadImage("firmes.png");
	monito_cadera = loadImage("cadera.png");
	monito_roll = new Animation("roll",17);
	monito_run = new Animation("run",12);
	monito_irun = new Animation("irun",12);
	monito_pin = loadImage("pin.png");
	monito_bell = loadImage("bell.png");

	puntos = new ArrayList();
	// Menu
	puntos.add(new Punto(M_WIDTH/2,M_HEIGHT/2,MODO_ROLL));
	puntos.add(new Punto(M_WIDTH/2,3*M_HEIGHT/2,MODO_CADERA));
	puntos.add(new Punto(M_WIDTH/2,5*M_HEIGHT/2,MODO_FIRMES));

	_modo = 0;
	escena = 1;


	imageMode(CENTER);
}

void mousePressed(){
}

void mouseReleased(){
	int i;
	boolean did_click;
	if(mouseX>M_WIDTH){
		if(do_clear){
			for(i=puntos.size()-1;i>m_menu;i--){
				puntos.remove(i);
			}

			do_clear = false;
		}

		did_click = false;
		for(i=m_menu+1;i<puntos.size();i++){
			if(puntos.get(i).click(mouseX,mouseY)){
				did_click = true;
			}
		}

		if(!did_click){
			puntos.add(new Punto(mouseX,mouseY,_modo));
		}
		
	}
	else{
		if(mouseY<M_HEIGHT){
			_modo = MODO_ROLL;
		} else if(mouseY<M_HEIGHT*2){
			_modo = MODO_CADERA;
		}
		else if (mouseY<M_HEIGHT*3){
			_modo = MODO_FIRMES;
			}
		else if (mouseY<M_HEIGHT*4 && m_menu==3){
			_modo = MODO_PIN;
			}
		else if(mouseY>M_HEIGHT*(m_menu+1)){ // salida
			for(i=m_menu+1;i<puntos.size();i++){
				puntos.get(i).salida();
			}
			do_clear= true;
			if(++escena == ESCENA_THRESHOLD){
				m_menu = 3;
//				puntos.add(new Punto(M_WIDTH/2,7*M_HEIGHT/2,MODO_PIN));
				_modo = MODO_PIN;
			}
		}

	}
		
}

void draw(){
	int i;

	background(BACKGROUND_COLOR);

	fill(color(10));
	rect(0,0,M_WIDTH,SCREEN_HEIGHT);

	image(monito_bell,M_WIDTH/2,((m_menu+1)*2+1)*M_HEIGHT/2,M_WIDTH,M_HEIGHT);

	if(m_menu==3){
		image(monito_pin,M_WIDTH/2,7*M_HEIGHT/2,M_WIDTH,M_HEIGHT);
	}

	for(i=0;i<puntos.size();i++){
		switch(puntos.get(i).modo){
			case MODO_ROLL:
				monito_roll.display(puntos.get(i).x,puntos.get(i).y);
				break;
			case MODO_CADERA:
				image(monito_cadera,puntos.get(i).x,puntos.get(i).y,M_WIDTH,M_HEIGHT);
				break;
			case MODO_FIRMES:
				image(monito_firmes,puntos.get(i).x,puntos.get(i).y,M_WIDTH,M_HEIGHT);
				break;
			case MODO_PIN:
				image(monito_pin,puntos.get(i).x,puntos.get(i).y,M_WIDTH,M_HEIGHT);
				break;			
			case MODO_RUN:
				monito_run.display(puntos.get(i).x,puntos.get(i).y);
				break;
			case MODO_IRUN:
				monito_irun.display(puntos.get(i).x,puntos.get(i).y);
				break;

		}
		puntos.get(i).updatepos();
	}
	monito_roll.nextFrame();
	monito_run.nextFrame();
	monito_irun.nextFrame();


	
}

class Punto{
	int x;
	int y;
	int modo;

	int xfinal;
	int yfinal;
	int modofinal;

	int INC_RUN = 10;
	int INC_CADERA = 10;
	int CLICK_RADIUS = 15;

	Punto(int _x, int _y, int _m){
		xfinal = _x;
		yfinal = _y;
		modofinal = _m;

			if(xfinal<SCREEN_WIDTH/2){
				modo = (modofinal==MODO_PIN) ? MODO_PIN : MODO_IRUN;
				x = SCREEN_WIDTH;
				y = yfinal;
			}
			else{
				modo = (modofinal==MODO_PIN) ? MODO_PIN : MODO_RUN;
				x = M_WIDTH;
				y = yfinal;
			}

	}

 	void updatepos(){
		switch(modo){
			case MODO_IRUN:
				if(x-INC_RUN<=xfinal){
					x = xfinal;
					modo = modofinal;
				}
				else{
					x-=INC_RUN;
				}
				break;
			case MODO_RUN:
				if(x+INC_RUN>=xfinal){
					x = xfinal;
					modo = modofinal;
				}
				else{
					x+=INC_RUN;
				}
				break;
			case MODO_PIN:
				if(x != xfinal){
					if(x<xfinal){
						if(x+INC_RUN>=xfinal){
							x=xfinal;
						}
						else{
							x+=INC_RUN;
						}
					}
					else{
						if(x-INC_RUN<=xfinal){
							x=xfinal;
						}
						else{
							x-=INC_RUN;
						}
					}
				}
				break;
		}
	}

	void salida(){
		if(modo!=MODO_PIN){
			if(x<SCREEN_WIDTH/2){
				modo = MODO_RUN;
				xfinal = SCREEN_WIDTH+M_WIDTH;
			}
			else{
				modo = MODO_IRUN;
				xfinal = -M_WIDTH;
			}
		}
		else{
			if(x<SCREEN_WIDTH/2){
				xfinal = SCREEN_WIDTH+M_WIDTH;
			}
			else{
				xfinal = -M_WIDTH;
			}
		}
	}

	boolean click(_x,_y){
		if(sqrt( sq(_x-x) + sq(_y-y) ) <= CLICK_RADIUS) { // update
			switch(modo){
				case MODO_ROLL:
					modo = MODO_FIRMES;
					break;
				case MODO_FIRMES:
					modo = MODO_ROLL;
					break;
				case MODO_CADERA:
					x += INC_CADERA;
					break;
			}
			return true;
		}
		return false;
	}
}



// Class for animating a sequence of GIFs



class Animation {

  PImage[] images;

    int imageCount;

      int frame;

        

	  Animation(String imagePrefix, int count) {

	      imageCount = count;

	          images = new PImage[imageCount];



		      for (int i = 0; i < imageCount; i++) {

		            // Use nf() to number format 'i' into four digits

			          String filename = imagePrefix + nf(i, 2) + ".png";

				        images[i] = loadImage(filename);

					    }

					      }



					        void display(float xpos, float ypos) {

					//	    frame = (frame+1) % imageCount;

						        image(images[frame], xpos, ypos,M_WIDTH,M_HEIGHT);

							  }

							    

							      int getWidth() {

							          return images[0].width;

								    }

		void nextFrame(){
  			frame = (frame+1) % imageCount;
			}

								    }


