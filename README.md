# igualaseis
Online game based in the choreography =6 by Benito González with the CEPRODAC

More information about the game [here](http://escenaconsejo.org/trayectoria/medios-digitales-interactivos/igualaseis-el-juego/)

More information about the choreography [here](http://igualaseis.ceprodac.com/)

The game can be found [here](http://sejomagno.org/igualaseis/)

Built with [Processing](http://processing.org)
